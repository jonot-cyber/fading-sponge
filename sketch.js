//this is a cool menger sponge that I made. tweak these vars please
let depth = 5
let probability = 9 //out of 10. Higher number == rarer

squareHolder = new Array()

class Square {
  constructor(x, y, s, r) {
    this.x = x
    this.y = y
    this.s = s
    this.r = r
    this.disp = true
    this.prand = false
  }
  split() {
    squareHolder.push(new Square(this.x,this.y,this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x + this.s / 3, this.y, this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x + this.s / 1.5, this.y, this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x,this.y + this.s / 3,this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x + this.s / 1.5,this.y + this.s / 3,this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x, this.y + this.s / 1.5, this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x + this.s / 3, this.y + this.s / 1.5, this.s / 3, this.r - 1))
    squareHolder.push(new Square(this.x + this.s / 1.5, this.y + this.s / 1.5, this.s / 3, this.r - 1))
    this.disp = false
  }
  splitb() {
    return true
  }
  draw() {
    if(this.disp) {
      square(this.x, this.y, this.s)
      if(this.r > 0 && random(10) > probability) {
        this.split()
      }
    }
  }
}

function setup() {
  createCanvas(400, 400);
  squareHolder.push(new Square(0,0,400, depth))
}

function draw() {
  noStroke()
  background(0);
  for(let square of squareHolder) {
    square.draw()
  }
}
